var socket = argument0;
var buffer = argument1;

var msgId = buffer_read(buffer, buffer_u8);

switch(msgId) {
	case net.join:
		var info = socketMap[? socket];
		buffer_seek(serverBuffer, buffer_seek_start, 0);
		buffer_write(serverBuffer, buffer_u8, net.join);
		buffer_write(serverBuffer, buffer_u8, socket);
		buffer_write(serverBuffer, buffer_u16, info[? "x"]);
		buffer_write(serverBuffer, buffer_u16, info[? "y"]);
		network_send_packet(socket, serverBuffer, buffer_tell(serverBuffer));
		break;
		
	case net.ping:
		var ms = buffer_read(buffer, buffer_s32);
		buffer_seek(serverBuffer, buffer_seek_start, 0);
		buffer_write(serverBuffer, buffer_u8, net.ping);
		buffer_write(serverBuffer, buffer_s32, ms);
		network_send_packet(socket, serverBuffer, buffer_tell(serverBuffer));
		break;
		
	case net.move:		
		var nx = buffer_read(buffer, buffer_u16);
		var ny = buffer_read(buffer, buffer_u16);
		buffer_seek(serverBuffer, buffer_seek_start, 0);
		buffer_write(serverBuffer, buffer_u8, net.move);
		buffer_write(serverBuffer, buffer_u16, nx);
		buffer_write(serverBuffer, buffer_s16, ny);
		network_send_packet(socket, serverBuffer, buffer_tell(serverBuffer));
		// Send to all
		break;
}