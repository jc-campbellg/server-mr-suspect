{
    "id": "e6f171f6-42c3-4dcd-8a2e-0982f7c4c208",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_server",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0234d48d-6aad-418d-bbdd-ad93bdc2110f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6f171f6-42c3-4dcd-8a2e-0982f7c4c208",
            "compositeImage": {
                "id": "d8c94390-a94a-4f22-ba97-4e4171dd5dd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0234d48d-6aad-418d-bbdd-ad93bdc2110f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4f6a79e-550b-465d-9f17-fd9f3a2af2ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0234d48d-6aad-418d-bbdd-ad93bdc2110f",
                    "LayerId": "8895d641-2aef-4338-b776-e93b7e84dc8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "8895d641-2aef-4338-b776-e93b7e84dc8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6f171f6-42c3-4dcd-8a2e-0982f7c4c208",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}