{
    "id": "8d9cd27e-c719-4c65-b36d-a5dc00745cec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_server",
    "eventList": [
        {
            "id": "38f3729b-4eca-4831-8684-31ef917a6ae1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d9cd27e-c719-4c65-b36d-a5dc00745cec"
        },
        {
            "id": "97d284df-18f8-4465-a880-de721afadc2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 7,
            "m_owner": "8d9cd27e-c719-4c65-b36d-a5dc00745cec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e6f171f6-42c3-4dcd-8a2e-0982f7c4c208",
    "visible": true
}