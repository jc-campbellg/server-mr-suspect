/// @description Start Server
enum net {
	join,
	ping,
	move,
	moveSync
};

network_create_server(network_socket_tcp, 6510, 10);

serverBuffer = buffer_create(2048, buffer_fixed, 1);

socketList = ds_list_create();
socketMap = ds_map_create();