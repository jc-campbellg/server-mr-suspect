/// @description Online Gaming
var typeEvent = async_load[? "type"];

switch(typeEvent) {
	case network_type_connect:
		var socket = async_load[? "socket"];
		ds_list_add(socketList, socket);
		var info = ds_map_create();
		spawnX = ds_list_size(socketList)*100;
		spawnY = ds_list_size(socketList) > 5 ? 200 : 100;
		show_debug_message(spawnX);
		info[? "x"] = spawnX;
		info[? "y"] = spawnY;
		ds_map_add(socketMap, socket, info);
		break;
		
	case network_type_disconnect:
		var socket = async_load[? "socket"];
		ds_list_delete(socketList, ds_list_find_index(socketList, socket));
		var info = socketMap[? socket];
		ds_map_destroy(info);
		ds_map_delete(socketMap, socket);
		break;
		
	case network_type_data:
		var socket = async_load[? "id"];
		var buffer = async_load[? "buffer"];
		handle_network_data(socket, buffer);
		break;
}